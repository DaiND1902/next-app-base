import { Component } from "react"
import { AdminLayout } from "/layouts/admin/_layoutAdmin"
import Head from 'next/head'

export default class Login extends Component {
    render() {
        return  <>
          <Head>
            <link rel="stylesheet" href="/assets/css/admin/login.css"/>
          </Head>
          <div className={"hold-transition login-page bg-img"}>
            <div className="login-box">
              {/*<div className="login-logo">
                <a href="/">Admin LTE Starting Kit <b>Nextjs</b></a>
              </div>*/}
              <div className="card">
                <div className="card-body login-card-body">
                  <div className="login-logo">
                    <h1><span>The Exploding</span></h1>

                    <div className="blobs_1"></div>
                    <div className="blobs_2"></div>
                    <div className="blobs_3"></div>
                    <div className="blobs_4"></div>
                    {/*<div class="blobs_5"></div>*/}
                    {/*<div class="blobs_6"></div>*/}
                    {/*<div class="blobs_7"></div>*/}
                  </div>
                  {/*<p className="login-box-msg">Sign In To Get Started</p>*/}
                  <form className="login-form">
                    <div className="input-group mb-3">
                      <input type="email" className="form-control" autoComplete="username" placeholder="Email" />
                      <div className="input-group-append">
                        <div className="input-group-text">
                          <span className="fa fa-user"></span>
                        </div>
                      </div>
                    </div>
                    <div className="input-group mb-3">
                      <input type="password" className="form-control" autoComplete="current-password" placeholder="Password" />
                      <div className="input-group-append">
                        <div className="input-group-text">
                          <span className="fa fa-lock"></span>
                        </div>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-8">
                        <div className="icheck-primary">
                          <input type="checkbox" id="remember" />
                          <label htmlFor="remember">Remember me</label>
                        </div>
                      </div>
                      <div className="col-4">
                        <button type="submit" className="btn btn-primary btn-block">Login</button>
                      </div>
                    </div>
                    <p className="mb-1">
                      <a href="#">I forgot my password</a>
                    </p>
                    <p className="mb-0">
                      <a href="#" className="text-center">Register</a>
                    </p>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </>
    }
}
