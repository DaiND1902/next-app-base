import Head from 'next/head'
import Image from 'next/image'
import dynamic from "next/dynamic"

import { AdminLayout } from "/layouts/admin/_layoutAdmin"

export async function getServerSideProps(context) {
  return {
    props: {}, // Will be passed to the page component as props
  }
}

export default function Home(props) {
  return (
    <AdminLayout contentTitle="dashboard">
    </AdminLayout>
  )
}
