import { Header } from './header'
import { Sidebar, ControlSidebar } from './sidebar'
import { Content } from './content'
import { Footer } from './footer'
import PropTypes from 'prop-types'
import { Component } from 'react'
import React from 'react'
import Head from 'next/head'

export class AdminLayout extends Component {
    render() {
        return <>
            <Head>
                <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet" />
                <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js" />
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" />
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" />
                <script src="/assets/js/adminlte.min.js" />
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
            </Head>
            <div className={"wrap"}>
                <Header />
                <Sidebar />
                <Content title={this.props.contentTitle} titleButton={this.props.contentTitleButton}>
                    {this.props.children}
                </Content>
                <ControlSidebar />
                <Footer rightContent={<div><b>Version:</b>1.0.0</div>} leftContent={'Copyright © 2020 Unit Zero All rights reserved.'} />
            </div>
        </>
    }
}

React.propTypes = {
    contentTitle: PropTypes.string,
    contentTitleButton: PropTypes.element,
    className: PropTypes.string,
    layoutType: PropTypes.bool,
    children: PropTypes.element,
};

React.propTypes = {
    className: 'container',
    contentTitle: null,
    contentTitleButton: null,
    children: null,
}
